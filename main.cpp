#include "moviesapp.h"

int main(int argc, char *argv[])
{
    return MoviesApp(argc, argv).exec();
}
