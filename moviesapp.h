#include <QCommandLineParser>
#include <QCoreApplication>
#include <QObject>
#include <QTextStream>

#ifndef MOVIESAPP_H
#define MOVIESAPP_H

class MoviesApp : public QObject {
    Q_OBJECT

    public:
        MoviesApp(int argc, char *argv[]);
        int exec();

        static QUrl create_url(const QString &title, const QString &type);
        static void show_info(const QJsonObject &json_object, QTextStream &text_stream);
        static void parse_response(const QByteArray &byte_array, QTextStream &text_stream);
        static int make_request(const QUrl &url, QTextStream &text_stream_stdout,
                                QTextStream &text_stream_stderr);
        static int search(const QString &title, const QString &type,
                          QTextStream &text_stream_stdout, QTextStream &text_stream_stderr);
    protected:
        void init();

    private:
        QCoreApplication core_application;
        QCommandLineParser command_line_parser;
        const QCommandLineOption search_option, type_option;
        QTextStream text_stream_stdout, text_stream_stderr;
};

#endif // MOVIESAPP_H
