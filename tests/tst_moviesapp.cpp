#include <QtTest>
#include "../moviesapp.h"

class TestMoviesApp : public QObject
{
    Q_OBJECT

    static void test_url(const QString &title);
    static void test_search(const QString &title, const QMap<QString, QString> &results);
private slots:
    void test_create_url();
    void test_make_request();
    void test_parse_response();
    void test_search();
};

const QString guardians_good = "Guardians of the Galaxy";
const QString guardians_bad = "Guardians of Galaxy";
const QStringList types = {"movie", "series", "episode"};

const QMap<QString, QString> responses_good({
    {"movie", "{\"Title\":\"Guardians of the Galaxy\",\"Year\":\"2014\",\"Rated\":\"PG-13\",\"Released\":\"01 Aug 2014\",\"Runtime\":\"121 min\",\"Genre\":\"Action, Adventure, Comedy, Sci-Fi\",\"Director\":\"James Gunn\",\"Writer\":\"James Gunn, Nicole Perlman, Dan Abnett (based on the Marvel comics by), Andy Lanning (based on the Marvel comics by), Bill Mantlo (character created by: Rocket Raccoon), Keith Giffen (character created by: Rocket Raccoon), Jim Starlin (characters created by: Drax the Destroyer,  Gamora & Thanos), Steve Englehart (character created by: Star-Lord), Steve Gan (character created by: Star-Lord), Steve Gerber (character created by: Howard the Duck), Val Mayerik (character created by: Howard the Duck)\",\"Actors\":\"Chris Pratt, Zoe Saldana, Dave Bautista, Vin Diesel\",\"Plot\":\"A group of intergalactic criminals must pull together to stop a fanatical warrior with plans to purge the universe.\",\"Language\":\"English\",\"Country\":\"USA\",\"Awards\":\"Nominated for 2 Oscars. Another 52 wins & 99 nominations.\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BMTAwMjU5OTgxNjZeQTJeQWpwZ15BbWU4MDUxNDYxODEx._V1_SX300.jpg\",\"Ratings\":[{\"Source\":\"Internet Movie Database\",\"Value\":\"8.1/10\"},{\"Source\":\"Rotten Tomatoes\",\"Value\":\"91%\"},{\"Source\":\"Metacritic\",\"Value\":\"76/100\"}],\"Metascore\":\"76\",\"imdbRating\":\"8.1\",\"imdbVotes\":\"913,669\",\"imdbID\":\"tt2015381\",\"Type\":\"movie\",\"DVD\":\"09 Dec 2014\",\"BoxOffice\":\"$270,592,504\",\"Production\":\"Walt Disney Pictures\",\"Website\":\"https://www.marvel.com/movies/guardians-of-the-galaxy\",\"Response\":\"True\"}"},
    {"series", "{\"Title\":\"Guardians of the Galaxy\",\"Year\":\"2015\xE2\x80\x93\",\"Rated\":\"TV-Y7\",\"Released\":\"26 Jul 2015\",\"Runtime\":\"22 min\",\"Genre\":\"Animation, Action, Adventure, Sci-Fi\",\"Director\":\"N/A\",\"Writer\":\"Dan Abnett, Andy Lanning, Marty Isenberg, Henry Gilroy\",\"Actors\":\"Kevin Michael Richardson, Vanessa Marshall, David Sobolov, Will Friedle\",\"Plot\":\"The adventures of a band of space warriors who work to protect the universe from the evil overlord Thanos.\",\"Language\":\"English\",\"Country\":\"USA\",\"Awards\":\"2 wins & 2 nominations.\",\"Poster\":\"https://m.media-amazon.com/images/M/MV5BNDM4NDQxMDU2MV5BMl5BanBnXkFtZTgwMDY2MDQ5NjE@._V1_SX300.jpg\",\"Ratings\":[{\"Source\":\"Internet Movie Database\",\"Value\":\"7.4/10\"}],\"Metascore\":\"N/A\",\"imdbRating\":\"7.4\",\"imdbVotes\":\"2,199\",\"imdbID\":\"tt4176370\",\"Type\":\"series\",\"totalSeasons\":\"3\",\"Response\":\"True\"}"},
    {"episode", "{\"Response\":\"False\",\"Error\":\"Movie not found!\"}"}
});

const QMap<QString, QString> responses_bad({
    {"movie", "{\"Response\":\"False\",\"Error\":\"Movie not found!\"}"},
    {"series", "{\"Response\":\"False\",\"Error\":\"Series not found!\"}"},
    {"episode", "{\"Response\":\"False\",\"Error\":\"Movie not found!\"}"}
});

const QMap<QString, QString> results_good({
    {"movie", "Title - Guardians of the Galaxy\nYear - 2014\nRated - PG-13\nReleased - 01 Aug 2014\nRuntime - 121 min\nGenre - Action, Adventure, Comedy, Sci-Fi\nDirector - James Gunn\nWriter - James Gunn, Nicole Perlman, Dan Abnett (based on the Marvel comics by), Andy Lanning (based on the Marvel comics by), Bill Mantlo (character created by: Rocket Raccoon), Keith Giffen (character created by: Rocket Raccoon), Jim Starlin (characters created by: Drax the Destroyer,  Gamora & Thanos), Steve Englehart (character created by: Star-Lord), Steve Gan (character created by: Star-Lord), Steve Gerber (character created by: Howard the Duck), Val Mayerik (character created by: Howard the Duck)\nActors - Chris Pratt, Zoe Saldana, Dave Bautista, Vin Diesel\nPlot - A group of intergalactic criminals must pull together to stop a fanatical warrior with plans to purge the universe.\nLanguage - English\nCountry - USA\nAwards - Nominated for 2 Oscars. Another 52 wins & 99 nominations.\nPoster - https://m.media-amazon.com/images/M/MV5BMTAwMjU5OTgxNjZeQTJeQWpwZ15BbWU4MDUxNDYxODEx._V1_SX300.jpg\nRatings - \nInternet Movie Database: 8.1/10\nRotten Tomatoes: 91%\nMetacritic: 76/100\nType - movie\nDVD - 09 Dec 2014\nBoxOffice - $270,592,504\nProduction - Walt Disney Pictures\nWebsite - https://www.marvel.com/movies/guardians-of-the-galaxy\n"},
    {"series", "Title - Guardians of the Galaxy\nYear - 2015–\nRated - TV-Y7\nReleased - 26 Jul 2015\nRuntime - 22 min\nGenre - Animation, Action, Adventure, Sci-Fi\nDirector - N/A\nWriter - Dan Abnett, Andy Lanning, Marty Isenberg, Henry Gilroy\nActors - Kevin Michael Richardson, Vanessa Marshall, David Sobolov, Will Friedle\nPlot - The adventures of a band of space warriors who work to protect the universe from the evil overlord Thanos.\nLanguage - English\nCountry - USA\nAwards - 2 wins & 2 nominations.\nPoster - https://m.media-amazon.com/images/M/MV5BNDM4NDQxMDU2MV5BMl5BanBnXkFtZTgwMDY2MDQ5NjE@._V1_SX300.jpg\nRatings - \nInternet Movie Database: 7.4/10\nType - series\n"},
    {"episode", "Movie not found!\n"}
});

const QMap<QString, QString> results_bad({
    {"movie", "Movie not found!\n"},
    {"series", "Series not found!\n"},
    {"episode", "Movie not found!\n"}
});

void TestMoviesApp::test_url(const QString &title)
{
    const QString base_url = "http://www.omdbapi.com?apikey=5e6aa81&t=" + title;
    {
        const QUrl url = MoviesApp::create_url(title, QString());
        QVERIFY(url.isValid());
        QCOMPARE(url.url(), base_url);
    }

    for(const auto &type : types) {
        const QUrl url = MoviesApp::create_url(title, type);
        QVERIFY(url.isValid());
        QCOMPARE(url.url(), base_url + "&type="+ type);
    }
}

void TestMoviesApp::test_create_url()
{
    test_url(guardians_good);
    test_url(guardians_bad);
}

void TestMoviesApp::test_parse_response()
{
    QString result;
    QTextStream text_stream(&result);
    for(const auto &type : types) {
        MoviesApp::parse_response(responses_good[type].toLocal8Bit(), text_stream);
        QCOMPARE(results_good[type], result);
        result.clear();
        MoviesApp::parse_response(responses_bad[type].toLocal8Bit(), text_stream);
        QCOMPARE(results_bad[type], result);
        result.clear();
    }
}

void TestMoviesApp::test_search(const QString &title, const QMap<QString, QString> &results)
{
    QString result_stdout, result_stderr;
    QTextStream text_stream_stdout(&result_stdout), text_stream_stderr(&result_stderr);
    MoviesApp::search(title, QString(), text_stream_stdout, text_stream_stderr);
    QCOMPARE(result_stdout, results["movie"]);
    QVERIFY(result_stderr.isEmpty());

    for(const auto &type : types) {
        result_stdout.clear();
        result_stderr.clear();

        MoviesApp::search(title, type, text_stream_stdout, text_stream_stderr);
        QCOMPARE(result_stdout, results[type]);
        QVERIFY(result_stderr.isEmpty());
    }
}

void TestMoviesApp::test_search()
{
    test_search(guardians_good, results_good);
    test_search(guardians_bad, results_bad);
}

void TestMoviesApp::test_make_request()
{
    {   // With no parameters
        QString result_stdout, result_stderr;
        QTextStream text_stream_stdout(&result_stdout), text_stream_stderr(&result_stderr);
        QUrl url("http://www.omdbapi.com?apikey=5e6aa81");
        MoviesApp::make_request(url, text_stream_stdout, text_stream_stderr);
        QCOMPARE(result_stdout, "Something went wrong.\n");
        QVERIFY(result_stderr.isEmpty());
    }

    {   // Not valid apikey
        QString result_stdout, result_stderr;
        QTextStream text_stream_stdout(&result_stdout), text_stream_stderr(&result_stderr);
        const QUrl url("http://www.omdbapi.com?apikey=notvalid");
        MoviesApp::make_request(url, text_stream_stdout, text_stream_stderr);
        QCOMPARE(result_stderr, "Host requires authentication\n");
        QVERIFY(result_stdout.isEmpty());
    }
}

QTEST_MAIN(TestMoviesApp)

#include "tst_moviesapp.moc"
