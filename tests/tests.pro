QT += testlib
QT -= gui
QT += network


CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

DEFINES += QT_NO_DEBUG_OUTPUT

TEMPLATE = app

SOURCES += \ 
    tst_moviesapp.cpp \
    ../moviesapp.cpp

HEADERS += \
    ../moviesapp.h
