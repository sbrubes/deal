#include "moviesapp.h"
#include <QDebug>
#include <QJsonDocument>
#include <QtNetwork>
#include <QTimer>

MoviesApp::MoviesApp(int argc, char *argv[])
    : core_application(argc, argv),
      command_line_parser(),
      search_option(QStringList() << "s" << "search", tr("Searches by media title"), "title"),
      type_option(QStringList() << "t" << "type",
                  tr("Specific type of media to search for: Movie/Series/Episode"), "type"),
      text_stream_stdout(stdout),
      text_stream_stderr(stderr)
{
    init();
}

int MoviesApp::exec()
{
    QTimer::singleShot(
        0,
        this,
        [this]()->void {
            QCoreApplication::exit(
                search(
                    command_line_parser.value(search_option),
                    command_line_parser.value(type_option),
                    text_stream_stdout, text_stream_stderr
                )
            );
        }
    );

    return core_application.exec();
}

QUrl MoviesApp::create_url(const QString &title, const QString &type)
{
    QUrl url("http://www.omdbapi.com");
    QUrlQuery url_query;
    url_query.addQueryItem("apikey", "5e6aa81");
    url_query.addQueryItem("t", title);

    if(!type.isEmpty())
        url_query.addQueryItem("type", type);

    url.setQuery(url_query);
    return url;
}

void MoviesApp::show_info(const QJsonObject &json_object, QTextStream &text_stream)
{
    const QList<QString> whitelist = {
        "Title", "Year", "Rated", "Released", "Runtime", "Genre", "Director", "Writer",
        "Actors", "Plot", "Language", "Country", "Awards", "Poster", "Ratings", "Type",
        "DVD", "BoxOffice", "Production", "Website"
    };

    for(const auto &key : whitelist) {
        if(json_object.contains(key)) {
            const QJsonValue json_value = json_object[key];
            if(json_value.isString())
                text_stream << key << " - " << json_value.toString() << endl;
            else if(key == "Ratings") {
                text_stream << key << " - " << endl;
                for(const auto rating_value : json_value.toArray()) {
                    const QJsonObject rating_json_object = rating_value.toObject();
                    text_stream << rating_json_object["Source"].toString() << ": " << rating_json_object["Value"].toString() << endl;
                }
            }
        }
    }
}

void MoviesApp::parse_response(const QByteArray &byte_array, QTextStream &text_stream)
{
    qDebug() << "Response:" << byte_array;
    QJsonObject json_object = QJsonDocument::fromJson(byte_array).object();

    if(json_object.contains("Error"))
        text_stream << json_object["Error"].toString() << endl;
    else
        show_info(json_object, text_stream);
}

int MoviesApp::make_request(const QUrl &url, QTextStream &text_stream_stdout, QTextStream &text_stream_stderr)
{
    QNetworkAccessManager network_access_manager;
    QNetworkRequest network_request(url);
    network_request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QNetworkReply * const network_reply = network_access_manager.get(network_request);

    // Synchronous wait to finish request
    QEventLoop loop;
    connect(network_reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    if(network_reply->error() == QNetworkReply::NetworkError::NoError) {
        parse_response(network_reply->readAll(), text_stream_stdout);
        network_reply->deleteLater();
        return EXIT_SUCCESS;
    }
    else {
        text_stream_stderr << network_reply->errorString() << endl;
        network_reply->deleteLater();
        return EXIT_FAILURE;
    }
}

int MoviesApp::search(const QString &title, const QString &type,
                      QTextStream &text_stream_stdout, QTextStream &text_stream_stderr)
{
    qDebug() << "Searching for" << title << "as" << type;
    QUrl url = create_url(title, type);
    qDebug() << url;
    return make_request(url, text_stream_stdout, text_stream_stderr);
}

void MoviesApp::init()
{
    QCoreApplication::setApplicationName("movies");
    QCoreApplication::setApplicationVersion("1.0.0");

    command_line_parser.setApplicationDescription(tr("Find informations about a movie\n  Usage: movies -s TITLE [-t] [TYPE]"));
    command_line_parser.addHelpOption();
    command_line_parser.addVersionOption();
    command_line_parser.addOption(search_option);
    command_line_parser.addOption(type_option);
    command_line_parser.process(core_application);
    if(!command_line_parser.isSet(search_option))
        command_line_parser.showHelp(EXIT_FAILURE);
}
